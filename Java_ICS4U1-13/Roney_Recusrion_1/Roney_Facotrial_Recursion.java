/*
 * Name : Roney Thomas
 * Program that finds factorial
 * for a given number using recursion
 */
import java.util.Scanner;

public class Roney_Facotrial_Recursion {

	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		System.out.println("Enter the input");
		System.out.println("Factorial = " + factorial(cin.nextInt()));
	}

	public static int factorial(int x) { // Gets the input and prints factorial
		if (x == 1) {
			return 1;
		}
		return x * factorial(x - 1); // Uses recursion 
	}

}