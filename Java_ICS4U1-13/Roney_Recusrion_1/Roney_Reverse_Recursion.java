/*
 * Name : Roney Thomas
 * Program that finds reverse of a string
 * using recursion
 */
import java.util.Scanner;

public class Roney_Reverse_Recursion {

  public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		System.out.println("Enter the string");
		System.out.println("The reverse order : "+reverse(cin.next())); // Gets the Input
	}

	public static String reverse (String x){ 
		if (x.length()==1){ //If the length is one returns the string
			return x;
		} 
		return x.substring(x.length()-1)+reverse(x.substring(0,x.length()-1)); //Calls reverse() with one less alphabet
	}
}