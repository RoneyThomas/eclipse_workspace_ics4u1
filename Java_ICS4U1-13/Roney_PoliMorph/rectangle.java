public class rectangle extends shapes {
	private int length;
	private int width;
	rectangle() {
		
	}

	rectangle(int w, int l) {
		this.setLength(l);
		this.setWidth(w);
	}

	rectangle(int s) {
		this.setLength(s);
		this.setWidth(s);
	}

	private void setLength(int l) {
		this.length=l;
	}

	private void setWidth(int w) {
		this.width=w;
	}

	public int getLength() {
		return this.length;
	}

	public int getWidth() {
		return this.width;
	}

	double getArea() {
		return getLength()*getWidth();
	}

	double getPerimeter() {
		return 2*(getLength()+getWidth());
	}
}