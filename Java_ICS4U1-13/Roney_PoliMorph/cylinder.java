public class cylinder extends circle {
	private int d;
	cylinder() {
		this(1,1);
	}

	cylinder(int r, int d) {
		super(r);
		this.setDepth(d);
	}

	private void setDepth(int d) {
		this.d = d;
	}

	private int getDepth() {
		return this.d;
	}

	public double getSurfaceArea() {
		return super.getSurfaceArea(super.getPerimeter()*this.d, 2*super.getArea());
	}

	public double getVolume() {
		return super.getVolume(super.getArea(), getDepth());
	}
}
