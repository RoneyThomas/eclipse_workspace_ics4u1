public class triangularPrism extends triangle {
	private int d;

	triangularPrism() {
		this(1,1,1,1,1);
	}

	triangularPrism(int a, int b, int c, int h, int d) {
		super(a, b, c, h);
		this.setDepth(d);
	}

	private void setDepth(int d) {
		this.d = d;
	}

	public int getDepth() {
		return this.d;
	}

	public double getSurfaceArea() {
		return super.getSurfaceArea(super.getPerimeter()*this.d, 2*super.getArea());
	}

	public double getVolume() {
		return super.getVolume(super.getArea(), getDepth());
	}
}
