public class triangle extends shapes{
	private int a;
	private int b;
	private int c;
	private int h;
	
	triangle() {

	}

	triangle(int a, int b, int c, int h) {
		this.setSideA(a);
		this.setSideB(b);
		this.setSideC(c);
		this.setHeight(h);
	}

	private void setSideA(int a) {
		this.a = a;
	}

	private void setSideB(int b) {
		this.b = b;
	}

	private void setSideC(int c) {
		this.c = c;
	}

	private void setHeight(int h) {
		this.h = h;
	}

	public int getSideA() {
		return this.a;
	}

	public int getSideB() {
		return this.b;
	}

	public int getSideC() {
		return this.c;
	}

	public int getHeight() {
		return this.h;
	}

	double getArea() {
		return 0.5*this.b*this.h;
	}

	double getPerimeter() {
		return this.getSideA()+this.getSideB()+this.getSideC();
	}
}
