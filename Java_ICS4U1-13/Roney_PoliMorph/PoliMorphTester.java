/**
 * @(#)tester.java
 *
 *
 * @author 
 * @version 1.00 2011/1/26
 */


public class PoliMorphTester {

    public static void main(String[]args){
     cylinder c1 = new cylinder();
     cylinder c2 = new cylinder(10, 5);
     
     rectangularPrism r1 = new rectangularPrism();
     rectangularPrism r2 = new rectangularPrism(2, 4, 6);
     
     rectangle square = new rectangle(5);
     
     triangularPrism t1 = new triangularPrism();
     triangularPrism t2 = new triangularPrism(2, 4, 6, 8, 10);
     
     System.out.println("Cylinder default SA: " + c1.getSurfaceArea() + " should be: 12.56");
     System.out.println("Cylinder default V: "+ c1.getVolume() + " should be: 3.14");
     
     System.out.println("Cylinder set SA: " + c2.getSurfaceArea() + " should be: 942");
     System.out.println("Cylinder set V: "+ c2.getVolume() + " should be: 1570");
     
     System.out.println("Rectangular default SA: " + r1.getSurfaceArea() + " should be: 6");
     System.out.println("Rectangular default V: "+ r1.getVolume() + " should be: 1");
     
     System.out.println("Rectangular set SA: " + r2.getSurfaceArea() + " should be: 88");
     System.out.println("Rectangular set V: "+ r2.getVolume() + " should be: 48");
     
     System.out.println("Triangular default SA: " + t1.getSurfaceArea() + " should be 4 or 3");
     System.out.println("Triangular default V: "+ t1.getVolume() + " should be: 0.5 or 0");
     
     System.out.println("Triangular set SA: " + t2.getSurfaceArea()+ " should be: 152");
     System.out.println("Triangular set V: "+ t2.getVolume()+ " should be: 160");
     
     System.out.println("Square A:" + square.getArea() + " should be: 25");
    }
    
    
}