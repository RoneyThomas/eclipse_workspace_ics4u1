public class circle extends shapes {
	private int r;
	circle() {
		
	}

	circle(int r) {
		this.setRadius(r);
	}

	private void setRadius(int r) {
		this.r = r;
	}

	public int getRadius() {
		return this.r;
	}

	public int getDiameter() {
		return this.r*2;
	}

	double getArea() {
		return Math.ceil(3.14*Math.pow(this.getRadius(), 2)*100)/100;
	}

	double getPerimeter() {
		return Math.ceil(2*3.14*this.getRadius()*100)/100;
	}
}
