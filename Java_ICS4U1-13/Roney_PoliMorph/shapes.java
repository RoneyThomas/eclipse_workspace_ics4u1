public abstract class shapes extends prisms {
	abstract double getArea();

	abstract double getPerimeter();
}
