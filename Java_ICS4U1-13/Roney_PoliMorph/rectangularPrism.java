public class rectangularPrism extends rectangle {
	private int d;
	rectangularPrism() {
		this(1,1,1);
	}

	rectangularPrism(int w, int l, int d) {
		super(w, l);
		this.setDepth(d);
	}

	private void setDepth(int d) {
		this.d = d;
	}

	public int getDepth() {
		return this.d;
	}

	public double getSurfaceArea() {
		return super.getSurfaceArea(2*(super.getWidth()*getDepth()+super.getLength()*super.getWidth()+super.getLength()*this.getDepth()),0);
	}

	public double getVolume() {
		return super.getVolume(super.getArea(), this.getDepth());
	}
}
