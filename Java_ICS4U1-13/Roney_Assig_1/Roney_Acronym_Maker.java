import java.util.Scanner;

public class Roney_Acronym_Maker {

	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		String x = cin.nextLine(); // Gets the input from the user
		if (!Character.isWhitespace(x.charAt(0))) { // Check if the first input
													// is space and prints it
			System.out.print(x.charAt(0));
		}
		for (int a = 0; a < x.length(); a++) { // Loops through the string and
												// checks for space
			if (Character.isWhitespace(x.charAt(a))
					&& !Character.isWhitespace(x.charAt(a + 1))) { // If there
																	// is space
																	// check the
																	// next to
																	// make sure
																	// its not
																	// space and
																	// prints
																	// the
																	// character
				System.out.print(x.charAt(a + 1));
			}
		}
	}

}
