import java.util.Scanner;

public class roney_assig_1_6 {

	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		System.out.println("Enter the string");
		int u = 0; // Vowel counter
		String x = cin.nextLine().toLowerCase(); // Gets the input and makes it
													// lowercase
		for (int a = 0; a < x.length(); a++) { // Checks the vowels by looping
			if (x.charAt(a) == 'a' || x.charAt(a) == 'e' || x.charAt(a) == 'i'
					|| x.charAt(a) == 'o' || x.charAt(a) == 'u'
					|| x.charAt(a) == 'y') {
				u++; // Increments if vowel is found
			}

		}
		System.out.println(u); // Prints the n.o of vowels
	}

}
