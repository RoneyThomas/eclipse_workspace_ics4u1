import java.util.Scanner;

public class roney_assig_1_8 {

	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		System.out.println("Enter the value for n");
		for (int x = 2, t = cin.nextInt(); x <= t; x++) { // Gets the input and loops
			int i = 0;
			int s = (int) Math.sqrt(x);
			for (int a = 2; a <= s; a++) {
				if (x % a == 0) { // If divisor is found it increments 
					i++;
				}
			}
			if (i == 0) { // Prints the number if its prime
				System.out.println(x);
			}
		}
	}
}