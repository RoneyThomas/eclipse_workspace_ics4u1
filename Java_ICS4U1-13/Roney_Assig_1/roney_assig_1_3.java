import java.util.Scanner;

public class roney_assig_1_3 {
	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		System.out.println("Enter the input");
		System.out.println("Factorial = " + factorial(cin.nextInt()));
	}

	public static int factorial(int x) { // Gets the input and prints factorial
		if (x == 1) {
			return 1;
		}
		return x * factorial(x - 1);
	}
}
