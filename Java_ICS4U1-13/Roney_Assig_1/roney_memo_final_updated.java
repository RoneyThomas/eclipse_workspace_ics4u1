import java.util.Scanner;

public class roney_memo_final_updated {
	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		String choice; // the choice of the user
		String[] item_Name = { "Dairy Milk", "KitKat", "Mars", "Snickers",
				"Aero" }; // the list of items
		int[] price = { 10, 20, 30, 40, 50 }; // array for price
		double total = 0; // The price of total purchase
		String lines = "------------------------------"; // Just for beauty!!!
		System.out.println(lines);
		for (int x = 0; x < 5; x++) {
			System.out.println(" Name : " + item_Name[x] + "; Price : "
					+ price[x]);
		}
		System.out.println(lines);
		for (int x = 0; x < 5; x++) {
			System.out.println("Would you like to buy " + item_Name[x]
					+ " (y/n)?");
			choice = cin.nextLine();
			if (choice.charAt(0) == 'y' || choice.charAt(0) == 'Y') {
				System.out.println("Enter the quantity ?");
				total += cin.nextInt() * price[x];
				cin.nextLine();// to remove the enter key
			}
		}
		System.out.println(); // For blank line
		System.out.println(lines + "\n" + lines);
		System.out.println("Subtotal ammount : " + total);
		total += total * 0.13;
		System.out.println("Total ammount (Subtotal+Tax) : " + total);
		System.out.println(lines + "\n" + lines);
		System.out.println("\n");
		if (total != 0) { // Calls check() only when the purchase is 0
			check(total, cin);
		}
	}

	public static void check(double total, Scanner cin) {
		System.out.println("Enter the ammount :");
		double amount = cin.nextDouble(); // Gets the users payment
		amount = total - amount; // total price - payment
		if (amount == 0) { // If the amount received is right
			System.out.println("Thank you and have a nice day");
		} else if (amount > 0) { // If the amount received is less
			System.out.println("You need to pay " + amount + "$ extra");
			check(amount, cin); // Recurs to get the payment
		} else { // if there is change
			change(amount * -1);
			System.out.println("Your change " + (Math.ceil(amount * 100) / 100)
					* -1 + "$"); // Rounds the number to two decimal places
		}
	}

	public static void change(double amount) { // Method for calculating change
		if (amount >= 20) { // Checks for number of 20$ bill
			int a = (int) (amount / 20);
			amount -= a * 20;
			System.out.println(a + " 20$");
		}
		if (amount >= 10) { // Checks for number of 10$
			int b = (int) (amount / 10);
			amount -= b * 10;
			System.out.println(b + " 10$");
		}
		if (amount >= 5) { // Checks for number of 5$
			int c = (int) (amount / 5);
			amount -= c * 5;
			System.out.println(c + " 5$");
		}
		if (amount >= 2) { // Checks for number of 2$
			int d = (int) (amount / 2);
			amount -= d * 2;
			System.out.println(d + " 2$");
		}
		if (amount >= 1) { // Checks for number of 1$
			int e = (int) (amount / 1);
			amount -= e * 1;
			System.out.println(e + " 1$");
		}
		if (amount >= 0.5) { // Checks for number of 0.5 Cent
			int f = (int) (amount / 0.5);
			amount -= f * 0.5;
			System.out.println(f + " 0.5 Cent");
		}
		if (amount >= 0.1) { // Checks for number of 0.1 Cent
			int g = (int) (amount / 0.1);
			amount -= g * 0.1;
			System.out.println(g + " 0.1 Cent");
		}
		if (amount >= 0.05) { // Checks for number of 0.05 Cent
			int h = (int) (amount / 0.05);
			amount -= h * 0.05;
			System.out.println(h + " 0.05 Cent");
		}
		if (amount >= 0.01) { // Checks for number of 0.01 Cent
			int i = (int) (amount / 0.01);
			amount -= i * 0.01;
			System.out.println(i + " 0.01 Cent");
		}
	}
}