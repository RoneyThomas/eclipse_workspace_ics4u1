import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class SolveMaze {

	List<String> map;
	Boolean[][] mind;
	List<Integer> possi = new ArrayList<Integer>();

	public SolveMaze() throws IOException {
		this("default.txt");
	}

	public SolveMaze(String x) throws IOException {
		this.map = Files.readAllLines(new File(x).toPath(),
				Charset.defaultCharset());
		mind = new Boolean[Integer.parseInt(this.map.get(1))][Integer
				.parseInt(this.map.get(0))];
		boolean s = false;
		for (int a = Integer.parseInt(this.map.get(1)), b = 0, e = 2; b < a; b++, e++) {
			for (int c = Integer.parseInt(this.map.get(0)), d = 0; d < c; d++) {
				if (map.get(e).charAt(d) == 'S') {
					if (map.get(e).charAt(d + 1) == 'O') {
						mind[e][d + 1] = true;
						find(e, d + 1, 'O');
					}
					if (map.get(e + 1).charAt(d) == 'O') {
						mind[e + 1][d] = true;
						find(e + 1, d, 'O');
					}
				}
			}
		}
	}

	private void find(int y, int x, char c) {
		this.possi.clear();
		if (map.get(y).charAt(x + 1) == c && !mind[y][x + 1]) {
			this.possi.add(y);
			this.possi.add(x + 1);
		}
		if (map.get(y).charAt(x - 1) == c && !mind[y][x - 1]) {
			this.possi.add(y);
			this.possi.add(x - 1);
		}
		if (map.get(y + 1).charAt(x) == c && !mind[y + 1][x]) {
			this.possi.add(y + 1);
			this.possi.add(x);
		}
		if (map.get(y + 1).charAt(x - 1) == c && !mind[y + 1][x - 1]) {
			this.possi.add(y + 1);
			this.possi.add(x - 1);
		}
		if (map.get(y + 1).charAt(x + 1) == c && !mind[y + 1][x + 1]) {
			this.possi.add(y + 1);
			this.possi.add(x + 1);
		}
		if (map.get(y - 1).charAt(x) == c && !mind[y - 1][x]) {
			this.possi.add(y - 1);
			this.possi.add(x);
		}
		if (map.get(y - 1).charAt(x - 1) == c && !mind[y - 1][x - 1]) {
			this.possi.add(y - 1);
			this.possi.add(x - 1);
		}
		if (map.get(y - 1).charAt(x + 1) == c && !mind[y - 1][x + 1]) {
			this.possi.add(y - 1);
			this.possi.add(x + 1);
		}
		if (!this.possi.isEmpty()) {
			
		}
	}
}
