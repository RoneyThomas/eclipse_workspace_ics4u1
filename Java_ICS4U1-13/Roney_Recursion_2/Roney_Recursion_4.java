/* Name : Roney Thomas
 * Program to convert base10 to binary
 */
import java.util.Scanner;

public class Roney_Recursion_4 {
	
	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		System.out.println("Enter the number");
		System.out.println(base(cin.nextInt()));
	}
	
	public static String base(int x){ // Converts base 10 to binary
		return (x==1) ? Integer.toString(1) : base((int)x/2)+x%2;
	}

}
