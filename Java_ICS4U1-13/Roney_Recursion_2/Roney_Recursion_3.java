/* Name : Roney Thomas
 * Program to find palidrome
 */
import java.util.Scanner;

public class Roney_Recursion_3 {

	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		System.out.println("Enter the string");
		System.out.println(pali(cin.next().toLowerCase()));
	}

	public static String pali(String x) { // Checks if a string passed is palidrome or not
		return (x.length() == 0 || x.length() == 1) ? "The string entered is a palidrome" : (x.charAt(0) == x.charAt(x.length() - 1) ? pali(x.substring(1, x.length() - 1)) : "Not palidrome");
	}

}
