/* Name : Roney Thomas
 * Binary search with recursion
 */
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;
import java.util.Scanner;
 
public class Roney_Recursion_5 {
	public static void main(String[] args) throws IOException {
		Scanner cin = new Scanner(System.in);
		System.out.println("Enter the word");
		List<String> x = Files.readAllLines(new File("words.txt").toPath(), Charset.defaultCharset()); // Gets all the words and stores in List
		System.out.println(bino(x,cin.next().toLowerCase(),0,x.size()-1)); // Passes the List and the word to find
	}
 
	public static int bino(List<String> x, String a, int min, int max){ // Method that does Binary search recursion
		int mid = (max+min)/2; // Finds the mid of the list
		return (max<min) ? -1 : (a.equals(x.get(mid))) ? mid : (x.get(mid).compareTo(a)>0) ? bino(x,a,min,mid-1) : bino(x,a,mid+1,max);
		/*
		 * If max is less than min returns -1
		 * If the middle word in list same as the word to find returns 0
		 * If the word to be found is less than the mid word then calls bino() again with min being mid+1
		 * If the word to be found is greater than the mid word then calls bino() again with max being mid-1
		 */
	}
}