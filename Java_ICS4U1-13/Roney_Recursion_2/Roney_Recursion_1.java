/* Name : Roney Thomas
 * Program to add all the number from 1 to n
 */
import java.util.Scanner;

public class Roney_Recursion_1 {

	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		System.out.println("Enter the number");
		System.out.println(powero(cin.nextInt()));
	}

	public static int powero(int x) { // Recursive function
		return (x==1) ? x : (x * x) + powero(x - 1); // Return the sum of n.o from 1 to n recurively
	}

}
