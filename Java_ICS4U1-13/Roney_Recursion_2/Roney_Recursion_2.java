/* Name : Roney Thomas
 * Find the GCD program
 */
import java.util.Scanner;

public class Roney_Recursion_2 {

	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		System.out.println("Enter the first & second number");
		System.out.println(gcd(cin.nextInt(), cin.nextInt())); // Gets the input
	}

	public static int gcd(int m, int n) {
		return (n == 0) ? m : gcd(n, m % n); // if n is zero returns m else does recursive calls to gcd()
	}

}
