/*
 * Name : Roney Thomas
 * Program : Maze Solving
 */
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

public class SolveMaze<E> {

	List<String> map;
	
	public SolveMaze() throws IOException { // Constructor that opens defaults.txt
		this("/home/roney/git/eclipse_workspace_ics4u1/Java_ICS4U1-13/Roney_Constructors_ToString/defaults.txt");
	}

	public SolveMaze(String x) throws IOException { // Constructor that opens defualts.txt from string passed to it
		this.map = Files.readAllLines(new File(x).toPath(),Charset.defaultCharset()); // Reads contents and makes a list
		Boolean[][] mind = new Boolean[Integer.parseInt(this.map.get(1))][Integer.parseInt(this.map.get(0))]; // 2d boolean array for storing the track
		for (Boolean[] control : mind){ // Fills it with default value
			Arrays.fill(control, Boolean.FALSE);
		}
		map.remove(0); // Removes the length
		map.remove(0); // Removes the width
		find(mind); // Invokes the method to find path
	}
	
	private void find(Boolean[][] track){
		int x = 0; // Varibale to store n.o of paths possible from a given point
		for (int a = 0;a<track.length;a++){ // For loop to find dead ends
			for(int b = 0;b<track[a].length;b++){
				if(map.get(a).charAt(b)=='O'){
					if(a-1>=0){
						if(map.get(a-1).charAt(b)=='O'||map.get(a-1).charAt(b)=='E'||map.get(a-1).charAt(b)=='S'||track[a-1][b]==true){
							x++;
						}
					}
					if(a+1<map.size()){
						if(map.get(a+1).charAt(b)=='O'||map.get(a+1).charAt(b)=='E'||map.get(a+1).charAt(b)=='S'||track[a+1][b]==true){
							x++;
						}
					}
					if(b-1>=0){
						if(map.get(a).charAt(b-1)=='O'||map.get(a).charAt(b-1)=='E'||map.get(a).charAt(b-1)=='S'||track[a][b-1]==true){
							x++;
						}
					}
					if(b+1<map.get(a).length()){
						if(map.get(a).charAt(b+1)=='O'||map.get(a).charAt(b+1)=='E'||map.get(a).charAt(b+1)=='S'||track[a][b+1]==true){
							x++;
						}
					}
				}
				if (x==1){ // If there is only one way i.e dead end
					track[a][b]=true; // makes the position true
					track = fixer(new int[] {a,b}, track); // Passes the dead end to fixer method
				}
				x=0;
			}
		}
		for (int a=0;a<track.length;a++){ // Prints the path
			for (int b=0;b<track[a].length;b++){
				if(track[a][b]==true){ // This is path dead end so turns it into 'O'
					System.out.print("O");
				} else if(map.get(a).charAt(b)=='O'){ // Prints the path
					System.out.print("-");
				} else { // Else print the character
					System.out.print(map.get(a).charAt(b));
				}
			}
			System.out.println();
		}
	}
	
	private Boolean[][] fixer(int[] x, Boolean[][] track) { // Method that removes dead from path
		int y = 0;
		int last [] = new int[2];
		if(x[0]-1>=0){
			if(map.get(x[0]-1).charAt(x[1])=='O'&&track[x[0]-1][x[1]]==false){
				y++;
				last[0]=x[0]-1;
				last[1]=x[1];
			}
		}
		if(x[0]+1<map.size()){
			if(map.get(x[0]+1).charAt(x[1])=='O'&&track[x[0]+1][x[1]]==false){
				y++;
				last[0]=x[0]+1;
				last[1]=x[1];
			}
		}
		if(x[1]-1>=0){
			if(map.get(x[0]).charAt(x[1]-1)=='O'&&track[x[0]][x[1]-1]==false){
				y++;
				last[0]=x[0];
				last[1]=x[1]-1;
			}
		}
		if(x[1]+1<map.get(x[0]).length()){
			if(map.get(x[0]).charAt(x[1]+1)=='O'&&track[x[0]][x[1]+1]==false){
				y++;
				last[0]=x[0];
				last[1]=x[1]+1;
			}
		}
		if (y==1){
			track[x[0]][x[1]]=true; // Marks the dead end and then recursively calls to fix other dead ends
			fixer(last, track); // Recursive call
		}
		return track;
	}
}