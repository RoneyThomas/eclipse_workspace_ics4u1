/*
 * Name : Roney Thomas
 * Program : Program to find Area, Volume, Height, Length, Width for a give shape (Box.java program)
 */
public class Box {
	private double length;
	private double width;
	private double height;
	public Box(double s) {
		this(s,s,s);
	}

	public Box(double w, double l, double h) {
		setWidth(w);
		setLength(l);
		setHeight(h);
	}

	private void setWidth(double x){
		this.width = x;
	}
	
	private void setLength(double x){
		this.length = x;
	}
	
	private void setHeight(double x){
		this.height = x;
	}
	
	public double getWidth(){
		return this.width;
	}
	
	public double getLength(){
		return this.length;
	}
	
	public double getHeight(){
		return this.height;
	}
	
	public double getArea(){
		return (2 * this.width * this.length) + (2 * this.width * this.height) + (2 * this.length * this.height);
	}
	
	public double getVolume(){
		return this.length*this.width*this.height;
	}
	
	public String toString(){
		return "Length = "+this.length+" Width = "+this.width+" Height = "+this.height;
	}
	
	public static Box boxes(Box box){
		Box newBox = new Box(box.width*1.25,box.length*1.25,box.height*1.25);
		return newBox;
	}
	
	public boolean nests(Box box){
		if (box.length<this.length&&box.width<this.width&&box.height<this.height){
			return true;
		} return false;
	}
	
}