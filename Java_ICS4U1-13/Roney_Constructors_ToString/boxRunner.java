/*
 * Name : Roney 
 * Program : Tester program for Box.java
 */
public class boxRunner{
	public static void main(String[] args){
		Box b1 = new Box(10, 15, 20);
		Box b2 = new Box(5);
		Box b3 = new Box(14);
		Box b4 = Box.boxes(b1);
		
		System.out.println(b4.getWidth() == 12.5);
		System.out.println(b4.getLength() == 18.75);
		System.out.println(b4.getHeight() == 25.0);
		
		System.out.println(b2.getArea() == 150.0);
		System.out.println(b3.getVolume() == 2744.0);
		
		System.out.println(b1.nests(b2) == true);
		System.out.println(b1.nests(b3) == false);
	}
}